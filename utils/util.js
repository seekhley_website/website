'use strict'
let config = require('../config/config'),
	nodemailer = require('nodemailer'),
	co = require('co'),
	l = require('../logger').root.child({'module': __filename.substring(__dirname.length + 1, __filename.length - 3)});


module.exports.sendEmailAsync = function(to, subject, body){
	process.nextTick(() => {
		co(function*(){
			// let emailRecordData = {
			// 	userId: userId,
			// 	reson: reason
			// };
			// let EmailRecord = mongoose.model('EmailRecord');
			// let newEmailRecord = yield new EmailRecord(emailRecordData).save(); 
			// body = body + newEmailRecord.token;
			return yield sendEmail(config.email.account, to, subject, body);
		}).then((res) => {
	        l.debug("util: send email res ", res);
	    }).catch((err) => {
	        l.error("util: error in sending email", err);
	    });
	});
}

let transporter;
function *sendEmail(from, to, subject, body){
	
	if(!transporter){
		transporter = nodemailer.createTransport({
			service : config.email.service,
		    auth : {
		      'user': config.email.account,
		      'pass': config.email.pass
		    }
		});
	}

	l.info("sending email to :", to);
// email sent validation + from
  	return yield new Promise((resolve, reject) => {
        var mailOptions = {
		    'to' : to,
		    'from' : "parth.mahajan@geminisolutions.in",
		    'subject' : subject,
		    'text' : body
  		};
        
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                l.error(error);
                reject(error);
            }
            l.info('email sent: %j', info.response);
            resolve(info.response);
        });
    });
}