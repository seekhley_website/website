'use strict'

let config = require('./config/config'),
	logger = require('./logger');

logger.init(config);

let l = logger.root.child({'module': __filename.substring(__dirname.length + 1, __filename.length - 3)});

process.on('uncaughtException', (err) => {
	l.error(err, "Uncaught Exception");
});

process.on('uncaughtRejection', ( err ) => {
	l.error(err, "Uncaught Rejection");
});

let koa = require('koa'),
	koaConfig = require('./config/koa'),
	co = require('co');

l.info('Initiating Koa');
try{
	let app = module.exports = new koa();
	koaConfig(app);

	app.init = co.wrap(function* (){
		l.info('Initiating server with configurations %j', config);
		app.server = app.listen(config.app.port);
	});

	if(!module.parent){
		return app.init();
	}

} catch(err){
	l.error( err, 'Server Error: ', err.message);
}

