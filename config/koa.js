'use strict'
let cors = require('koa-cors'),
	responseTime = require('koa-response-time'),
	l = require('./../logger').root,
	fs = require('fs'),
	swig = require('koa-swig'),
	statics = require('koa-static'),
	path = require('path'),
	config = require('./../config/config'),
	route = require('koa-router');

module.exports = function(app){
	//cross origin attack
	app.use(cors({
		maxAge: 1000,
		credentials: true,
		methods: 'GET, HEAD, OPTIONS, PUT, POST, DELETE',
		header: 'Access-Control-Allow-Origin, Access-Control-Expose-Headers, Origin, X-Requested-With, Content-Type, Accept, Authorzation'
	}));

	// publicly accessible folder
	app.use(statics(path.join(__dirname, '../', '/public')));

	//Configuration to render swig 
	app.context.render = swig({
		root: config.views.root,
		autoescape: true,
		cache: false, 
		ext: config.views.ext
	});

	app.use(responseTime());

	let router = route(app);

	fs.readdirSync(__dirname + '/../controllers').forEach(function(file){
		console.log(file.indexOf('js'));
		
		if(~file.indexOf('js')){
			let controller = require(__dirname + './../controllers/' + file).init(router);
		}	
	});

	app.use(router.routes());
}