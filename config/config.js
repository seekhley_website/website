'use strict'

let path = require('path');

let platformConfig = {
	log: {
		level: 'debug',
		path: __dirname + '/../logs/seekhley.log'
	},
	app: {
		port: 80,
		serverpath: 'http://127.0.0.1'
	},
	views: {
		root: path.join(__dirname, '../', '/views'),
		ext: 'html'
	},
	email: {
		service: 'GMAIL',
		account: 'parth.mahajan@seekhley.com',
		pass: 'oo1@GMAIL33'
	},
	emailMessages: {
		subject: 'Seekhley',
		activateAccount: 'Dear User \nThank You for signing up. \nPlease activate your account using following link\nhttp://127.0.0.1:3000/resetPassword?id=',
		resetPassword: 'Dear User \nPlease follow the below link to rest passsword \nhttp://127.0.0.1:3000/resetPassword?id=',
	}
}

module.exports = platformConfig;
