'use stict'
exports.init = function(app){
	app.get('/', renderIndex);
}

function *renderIndex(next){
	try{

		yield this.render('index');
		this.status = 200;
	} catch(next){

		this.status = err.status || 500;
		this.body = err.message;
		this.app.emit('error', err, this);
	}
}