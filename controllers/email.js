'use strict'

let parse = require('koa-better-body'),
	util = require('../utils/util'),
	config = require('../config/config');

exports.init = function(app){
	app.post('/sendEmail', parse(), sendEmail);
}

function *sendEmail(next){
	try{

		
		let userReq = this.request.body.fields;
		if(!userReq){
			this.throw('no data found', 400);
		}

		if(userReq.status == 1){
			var message = "name = " + userReq.name + "\n email = " + userReq.email + " \n message = " + userReq.message;
			util.sendEmailAsync(userReq.email, 'Thank You', "Respected Sir\/ Ma'am,\n\nThank you for showing your interest in Seekhley. We shall get back to you shortly with all the details.\nSincerely \nThe Seekhley Team");
			util.sendEmailAsync(config.email.account, 'User Contacted', message);
		}
		if(userReq.status == 2){
			var message = "name = " + userReq.name + "School Name = " + userReq.schoolName + "\n email = " + userReq.email + " \n phone Number = " + userReq.userPhone;
			util.sendEmailAsync(userReq.email, 'Thank You', "Respected Sir\/ Ma'am,\n\nThank you for showing your interest in Seekhley. We shall get back to you shortly with all the details.\nSincerely \nThe Seekhley Team");
			util.sendEmailAsync(config.email.account, 'User Contacted', message);

		}
		this.status = 200;
	} catch(err){
		this.body = err.message;
		this.status = err.status || 500;
		this.app.emit('error', err, this);

	}
}